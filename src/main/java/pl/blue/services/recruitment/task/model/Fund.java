package pl.blue.services.recruitment.task.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@AllArgsConstructor
public class Fund {

  private Long id;
  private Type type;
  private String name;

  @AllArgsConstructor
  public enum Type {
    POLISH("Polskie", "Polski"),
    FOREIGN("Zagraniczne", "Zagraniczny"),
    CASH("Pieniężne", "Pieniężny");

    @Getter private final String type;
    @Getter private final String fundName;
  }

  @AllArgsConstructor
  public enum Investment {
    SAFE("bezpieczny"),
    BALANCED("zrównoważony"),
    AGGRESSIVE("agresywny");

    @Getter private final String name;
  }
}
