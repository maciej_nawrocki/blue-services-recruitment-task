package pl.blue.services.recruitment.task.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
public class Result {
  List<FundDistribution> distributions;
  BigDecimal reminder;
}
