package pl.blue.services.recruitment.task.fund;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.blue.services.recruitment.task.model.Fund;

import java.util.EnumMap;
import java.util.Map;

@Service
public class FundProperties {

  @Value("#{new Double(${funds.safe.polish})}")
  private double safePolish;

  @Value("#{new Double(${funds.safe.foreign})}")
  private double safeForeign;

  @Value("#{new Double(${funds.safe.cash})}")
  private double safeCash;

  @Value("#{new Double(${funds.balanced.polish})}")
  private double balancedPolish;

  @Value("#{new Double(${funds.balanced.foreign})}")
  private double balancedForeign;

  @Value("#{new Double(${funds.balanced.cash})}")
  private double balancedCash;

  @Value("#{new Double(${funds.aggressive.polish})}")
  private double aggressivePolish;

  @Value("#{new Double(${funds.aggressive.foreign})}")
  private double aggressiveForeign;

  @Value("#{new Double(${funds.aggressive.cash})}")
  private double aggressiveCash;

  Map<Fund.Type, Double> getFundTypesPartsForInvestment(Fund.Investment investment) {
    Map<Fund.Type, Double> investmentFundTypesParts = new EnumMap<>(Fund.Type.class);
    switch (investment) {
      case SAFE:
        investmentFundTypesParts.put(Fund.Type.POLISH, safePolish);
        investmentFundTypesParts.put(Fund.Type.FOREIGN, safeForeign);
        investmentFundTypesParts.put(Fund.Type.CASH, safeCash);
        break;
      case BALANCED:
        investmentFundTypesParts.put(Fund.Type.POLISH, balancedPolish);
        investmentFundTypesParts.put(Fund.Type.FOREIGN, balancedForeign);
        investmentFundTypesParts.put(Fund.Type.CASH, balancedCash);
        break;
      case AGGRESSIVE:
        investmentFundTypesParts.put(Fund.Type.POLISH, aggressivePolish);
        investmentFundTypesParts.put(Fund.Type.FOREIGN, aggressiveForeign);
        investmentFundTypesParts.put(Fund.Type.CASH, aggressiveCash);
        break;
    }
    return investmentFundTypesParts;
  }
}
