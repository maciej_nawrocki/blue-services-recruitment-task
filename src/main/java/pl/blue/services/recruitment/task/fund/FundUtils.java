package pl.blue.services.recruitment.task.fund;

import pl.blue.services.recruitment.task.model.Fund;
import pl.blue.services.recruitment.task.model.FundDistribution;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

final class FundUtils {

  static Map<Fund.Type, Integer> countFundTypes(List<Fund> funds) {
    Map<Fund.Type, Integer> numbersOfFunds = new EnumMap<>(Fund.Type.class);
    funds.forEach(fund -> incrementValue(numbersOfFunds, fund.getType()));
    return numbersOfFunds;
  }

  private static <T> void incrementValue(Map<T, Integer> map, T key) {
    AtomicInteger atomic = new AtomicInteger(map.getOrDefault(key, 0));
    map.put(key, atomic.incrementAndGet());
  }

  static void addValueToPercentage(FundDistribution fundDistribution, double value) {
    fundDistribution.setPercent(fundDistribution.getPercent() + value);
  }

  static double roundValueFloor(double value) {
    return Math.floor(value * 100) / 100;
  }
}
