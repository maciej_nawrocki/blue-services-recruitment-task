package pl.blue.services.recruitment.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlueServicesRecruitmentTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlueServicesRecruitmentTaskApplication.class, args);
    }

}
