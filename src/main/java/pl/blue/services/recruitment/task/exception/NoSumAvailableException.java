package pl.blue.services.recruitment.task.exception;

public class NoSumAvailableException extends RuntimeException {

  private static final long serialVersionUID = -7583296846606452295L;
  private static final String MESSAGE = "No sum available after reducing stream.";

  @Override
  public String getMessage() {
    return MESSAGE;
  }
}
