package pl.blue.services.recruitment.task.fund;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.blue.services.recruitment.task.exception.NoSumAvailableException;
import pl.blue.services.recruitment.task.model.Fund;
import pl.blue.services.recruitment.task.model.FundDistribution;
import pl.blue.services.recruitment.task.model.Result;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class FundsRecasts {

  private final @NonNull FundCalculator fundCalculator;

  static BigDecimal calculateAmountsAndSum(
      BigDecimal amount, List<FundDistribution> distributions) {
    return distributions.stream()
        .peek(
            distribution ->
                distribution.setAmount(
                    amount
                        .multiply(BigDecimal.valueOf(distribution.getPercent() / 100))
                        .setScale(0, RoundingMode.DOWN)))
        .map(FundDistribution::getAmount)
        .reduce(BigDecimal::add)
        .orElseThrow(NoSumAvailableException::new);
  }

  Result getDistributionsForFunds(List<Fund> funds, Fund.Investment investment, BigDecimal amount) {
    List<FundDistribution> distributions =
        fundCalculator.getDistributionsWithCalculatedPercents(funds, investment);
    BigDecimal sum = calculateAmountsAndSum(amount, distributions);
    return new Result(distributions, amount.subtract(sum));
  }
}
