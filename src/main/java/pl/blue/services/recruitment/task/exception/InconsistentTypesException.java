package pl.blue.services.recruitment.task.exception;

public class InconsistentTypesException extends RuntimeException {
  private static final long serialVersionUID = -6487757677388303503L;

  private static final String MESSAGE = "Cannot find first element in stream.";

  @Override
  public String getMessage() {
    return MESSAGE;
  }
}
