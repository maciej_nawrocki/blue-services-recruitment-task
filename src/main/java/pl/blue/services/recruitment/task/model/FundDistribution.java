package pl.blue.services.recruitment.task.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class FundDistribution {

  private Long id;
  private Fund.Type type;
  private String name;
  private BigDecimal amount;
  private double percent;

  public FundDistribution(Fund fund, BigDecimal amount, double percent) {
    this.id = fund.getId();
    this.type = fund.getType();
    this.name = fund.getName();
    this.amount = amount;
    this.percent = percent;
  }
}
