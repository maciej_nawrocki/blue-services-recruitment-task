package pl.blue.services.recruitment.task.fund;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.blue.services.recruitment.task.exception.NoFundOfThatTypeException;
import pl.blue.services.recruitment.task.model.Fund;
import pl.blue.services.recruitment.task.model.FundDistribution;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FundCalculator {

  private final @NonNull FundProperties fundProperties;

  private static List<FundDistribution> collectDistributionsWithEstimatedPercentages(
      List<Fund> funds, Map<Fund.Type, Double> fundsEstimatedPercentages) {
    return funds.stream()
        .map(
            fund -> new FundDistribution(fund, null, fundsEstimatedPercentages.get(fund.getType())))
        .collect(Collectors.toList());
  }

  private static FundDistribution getFirstOfType(
      List<FundDistribution> distributions, Fund.Type type) {
    return distributions.stream()
        .filter(distribution -> type == distribution.getType())
        .findFirst()
        .orElseThrow(() -> new NoFundOfThatTypeException(type));
  }

  private static Map<Fund.Type, Double> collectPercentageReminders(
      List<FundDistribution> distributions, Map<Fund.Type, Double> fundTypesPartsForInvestment) {
    return fundTypesPartsForInvestment.entrySet().stream()
        .collect(
            Collectors.toMap(
                Map.Entry::getKey,
                entry ->
                    entry.getValue() % getFirstOfType(distributions, entry.getKey()).getPercent()));
  }

  List<FundDistribution> fillFundsPercentagesIfIncomplete(
      List<FundDistribution> distributions, Map<Fund.Type, Double> fundTypesPartsForInvestment) {
    Map<Fund.Type, Double> percentageRemainders =
        collectPercentageReminders(distributions, fundTypesPartsForInvestment);
    Arrays.stream(Fund.Type.values())
        .forEach(
            type ->
                FundUtils.addValueToPercentage(
                    getFirstOfType(distributions, type), percentageRemainders.get(type)));
    return distributions;
  }

  List<FundDistribution> getDistributionsWithCalculatedPercents(
      List<Fund> funds, Fund.Investment investment) {
    Map<Fund.Type, Integer> amountsOfFundTypes = FundUtils.countFundTypes(funds);
    Map<Fund.Type, Double> fundTypesPartsForInvestment =
        fundProperties.getFundTypesPartsForInvestment(investment);
    Map<Fund.Type, Double> fundsEstimatedPercentages =
        estimatePercentages(fundTypesPartsForInvestment, amountsOfFundTypes);
    List<FundDistribution> distributions =
        collectDistributionsWithEstimatedPercentages(funds, fundsEstimatedPercentages);
    distributions = fillFundsPercentagesIfIncomplete(distributions, fundTypesPartsForInvestment);
    return distributions;
  }

  Map<Fund.Type, Double> estimatePercentages(
      Map<Fund.Type, Double> investmentFundTypesParts, Map<Fund.Type, Integer> fundTypesAmounts) {
    Map<Fund.Type, Double> fundsEstimatedPercentages = new EnumMap<>(Fund.Type.class);
    fundTypesAmounts.forEach(
        (key, value) ->
            fundsEstimatedPercentages.put(
                key, FundUtils.roundValueFloor(investmentFundTypesParts.get(key) / value)));
    return fundsEstimatedPercentages;
  }
}
