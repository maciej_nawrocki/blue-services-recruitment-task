package pl.blue.services.recruitment.task.fund;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.blue.services.recruitment.task.model.Fund;
import pl.blue.services.recruitment.task.model.FundDistribution;
import pl.blue.services.recruitment.task.model.Result;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FundsRecastsTests {

  private static List<Fund> inputFunds;
  private static List<Fund> inputFunds2;
  @Autowired private FundsRecasts fundsRecasts;

  @BeforeClass
  public static void setup() {
    inputFunds = new ArrayList<>();
    inputFunds.add(new Fund(1L, Fund.Type.POLISH, "Fundusz Polski 1"));
    inputFunds.add(new Fund(2L, Fund.Type.POLISH, "Fundusz Polski 2"));
    inputFunds.add(new Fund(3L, Fund.Type.FOREIGN, "Fundusz Zagraniczny 1"));
    inputFunds.add(new Fund(4L, Fund.Type.FOREIGN, "Fundusz Zagraniczny 2"));
    inputFunds.add(new Fund(5L, Fund.Type.FOREIGN, "Fundusz Zagraniczny 3"));
    inputFunds.add(new Fund(6L, Fund.Type.CASH, "Fundusz Pieniężny 1"));

    inputFunds2 = new ArrayList<>();
    inputFunds2.add(new Fund(1L, Fund.Type.POLISH, "Fundusz Polski 1"));
    inputFunds2.add(new Fund(2L, Fund.Type.POLISH, "Fundusz Polski 2"));
    inputFunds2.add(new Fund(3L, Fund.Type.POLISH, "Fundusz Polski 3"));
    inputFunds2.add(new Fund(4L, Fund.Type.FOREIGN, "Fundusz Zagraniczny 1"));
    inputFunds2.add(new Fund(5L, Fund.Type.FOREIGN, "Fundusz Zagraniczny 2"));
    inputFunds2.add(new Fund(6L, Fund.Type.CASH, "Fundusz Pieniężny 1"));
  }

  @Test
  public void calculateAmountsAndSumTest() {
    List<FundDistribution> inputDistributions = new ArrayList<>();
    inputDistributions.add(new FundDistribution(inputFunds.get(0), null, 3.34));
    inputDistributions.add(new FundDistribution(inputFunds.get(1), null, 6.66));
    inputDistributions.add(new FundDistribution(inputFunds.get(2), null, 90));

    List<FundDistribution> expectedAmounts = new ArrayList<>();
    expectedAmounts.add(new FundDistribution(inputFunds.get(0), new BigDecimal("334"), 3.34));
    expectedAmounts.add(new FundDistribution(inputFunds.get(1), new BigDecimal("666"), 6.66));
    expectedAmounts.add(new FundDistribution(inputFunds.get(2), new BigDecimal("9000"), 90));

    assertEquals(
        new BigDecimal("10000"),
        FundsRecasts.calculateAmountsAndSum(new BigDecimal("10000"), inputDistributions));

    assertThat(expectedAmounts, is(inputDistributions));
  }

  @Test
  public void pickedFundsTest() {
    List<FundDistribution> expectedDistributions = new ArrayList<>();
    expectedDistributions.add(new FundDistribution(inputFunds.get(0), new BigDecimal("1000"), 10));
    expectedDistributions.add(new FundDistribution(inputFunds.get(1), new BigDecimal("1000"), 10));
    expectedDistributions.add(new FundDistribution(inputFunds.get(2), new BigDecimal("2500"), 25));
    expectedDistributions.add(new FundDistribution(inputFunds.get(3), new BigDecimal("2500"), 25));
    expectedDistributions.add(new FundDistribution(inputFunds.get(4), new BigDecimal("2500"), 25));
    expectedDistributions.add(new FundDistribution(inputFunds.get(5), new BigDecimal("500"), 5));

    Result expectedResult = new Result(expectedDistributions, new BigDecimal("0"));
    assertThat(
        expectedResult,
        is(
            fundsRecasts.getDistributionsForFunds(
                inputFunds, Fund.Investment.SAFE, new BigDecimal(10000))));
  }

  @Test
  public void pickedFundsTestUnallocatedAmount() {
    List<FundDistribution> expectedDistributions = new ArrayList<>();
    expectedDistributions.add(new FundDistribution(inputFunds.get(0), new BigDecimal("1000"), 10));
    expectedDistributions.add(new FundDistribution(inputFunds.get(1), new BigDecimal("1000"), 10));
    expectedDistributions.add(new FundDistribution(inputFunds.get(2), new BigDecimal("2500"), 25));
    expectedDistributions.add(new FundDistribution(inputFunds.get(3), new BigDecimal("2500"), 25));
    expectedDistributions.add(new FundDistribution(inputFunds.get(4), new BigDecimal("2500"), 25));
    expectedDistributions.add(new FundDistribution(inputFunds.get(5), new BigDecimal("500"), 5));

    Result expectedResult = new Result(expectedDistributions, new BigDecimal("1"));
    assertThat(
        expectedResult,
        is(
            fundsRecasts.getDistributionsForFunds(
                inputFunds, Fund.Investment.SAFE, new BigDecimal(10001))));
  }

  @Test
  public void pickedFundsTestIncompleteAmount() {
    List<FundDistribution> expectedDistributions = new ArrayList<>();
    expectedDistributions.add(
        new FundDistribution(inputFunds2.get(0), new BigDecimal("668"), 6.68));
    expectedDistributions.add(
        new FundDistribution(inputFunds2.get(1), new BigDecimal("666"), 6.66));
    expectedDistributions.add(
        new FundDistribution(inputFunds2.get(2), new BigDecimal("666"), 6.66));
    expectedDistributions.add(
        new FundDistribution(inputFunds2.get(3), new BigDecimal("3750"), 37.5));
    expectedDistributions.add(
        new FundDistribution(inputFunds2.get(4), new BigDecimal("3750"), 37.5));
    expectedDistributions.add(new FundDistribution(inputFunds2.get(5), new BigDecimal("500"), 5));

    Result expectedResult = new Result(expectedDistributions, new BigDecimal("0"));
    assertThat(
        expectedResult,
        is(
            fundsRecasts.getDistributionsForFunds(
                inputFunds2, Fund.Investment.SAFE, new BigDecimal(10000))));
  }
}
