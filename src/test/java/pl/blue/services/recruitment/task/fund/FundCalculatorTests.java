package pl.blue.services.recruitment.task.fund;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.blue.services.recruitment.task.model.Fund;
import pl.blue.services.recruitment.task.model.FundDistribution;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FundCalculatorTests {

  private static final double DELTA = 0.001;

  private static Map<Fund.Type, Double> fundTypesPartsForInvestment;
  private static List<Fund> fundsType1;
  private static List<Fund> fundsType2;

  @Autowired private FundCalculator fundCalculator;

  @BeforeClass
  public static void setup() {
    fundTypesPartsForInvestment = new EnumMap<>(Fund.Type.class);
    fundTypesPartsForInvestment.put(Fund.Type.POLISH, 20.0);
    fundTypesPartsForInvestment.put(Fund.Type.FOREIGN, 75.0);
    fundTypesPartsForInvestment.put(Fund.Type.CASH, 5.0);

    fundsType1 = new ArrayList<>();
    fundsType1.add(new Fund(1L, Fund.Type.POLISH, "Fundusz Polski 1"));
    fundsType1.add(new Fund(2L, Fund.Type.POLISH, "Fundusz Polski 2"));
    fundsType1.add(new Fund(3L, Fund.Type.FOREIGN, "Fundusz Zagraniczny 1"));
    fundsType1.add(new Fund(4L, Fund.Type.FOREIGN, "Fundusz Zagraniczny 2"));
    fundsType1.add(new Fund(5L, Fund.Type.FOREIGN, "Fundusz Zagraniczny 3"));
    fundsType1.add(new Fund(6L, Fund.Type.CASH, "Fundusz Pieniężny 1"));

    fundsType2 = new ArrayList<>();
    fundsType2.add(new Fund(1L, Fund.Type.POLISH, "Fundusz Polski 1"));
    fundsType2.add(new Fund(2L, Fund.Type.POLISH, "Fundusz Polski 2"));
    fundsType2.add(new Fund(3L, Fund.Type.POLISH, "Fundusz Polski 3"));
    fundsType2.add(new Fund(4L, Fund.Type.FOREIGN, "Fundusz Zagraniczny 1"));
    fundsType2.add(new Fund(5L, Fund.Type.FOREIGN, "Fundusz Zagraniczny 2"));
    fundsType2.add(new Fund(6L, Fund.Type.CASH, "Fundusz Pieniężny 1"));
  }

  @Test
  public void estimatePercentagesTest() {
    Map<Fund.Type, Integer> fundTypesAmounts = new EnumMap<>(Fund.Type.class);
    fundTypesAmounts.put(Fund.Type.POLISH, 2);
    fundTypesAmounts.put(Fund.Type.FOREIGN, 3);
    fundTypesAmounts.put(Fund.Type.CASH, 1);
    Map<Fund.Type, Double> estimatedPercentages =
        fundCalculator.estimatePercentages(fundTypesPartsForInvestment, fundTypesAmounts);
    assertEquals(10, estimatedPercentages.get(Fund.Type.POLISH), DELTA);
    assertEquals(25, estimatedPercentages.get(Fund.Type.FOREIGN), DELTA);
    assertEquals(5, estimatedPercentages.get(Fund.Type.CASH), DELTA);
  }

  @Test
  public void estimateNonIntegerPercentagesTest() {
    Map<Fund.Type, Integer> fundTypesAmounts = new EnumMap<>(Fund.Type.class);
    fundTypesAmounts.put(Fund.Type.POLISH, 3);
    fundTypesAmounts.put(Fund.Type.FOREIGN, 2);
    fundTypesAmounts.put(Fund.Type.CASH, 1);

    Map<Fund.Type, Double> estimatedPercentages =
        fundCalculator.estimatePercentages(fundTypesPartsForInvestment, fundTypesAmounts);
    assertEquals(6.66, estimatedPercentages.get(Fund.Type.POLISH), DELTA);
    assertEquals(37.5, estimatedPercentages.get(Fund.Type.FOREIGN), DELTA);
    assertEquals(5, estimatedPercentages.get(Fund.Type.CASH), DELTA);
  }

  @Test
  public void calculatePercentsTest() {
    List<FundDistribution> expectedDistributions = new ArrayList<>();
    expectedDistributions.add(new FundDistribution(fundsType1.get(0), null, 10.0));
    expectedDistributions.add(new FundDistribution(fundsType1.get(1), null, 10.0));
    expectedDistributions.add(new FundDistribution(fundsType1.get(2), null, 25.0));
    expectedDistributions.add(new FundDistribution(fundsType1.get(3), null, 25.0));
    expectedDistributions.add(new FundDistribution(fundsType1.get(4), null, 25.0));
    expectedDistributions.add(new FundDistribution(fundsType1.get(5), null, 5.0));

    assertThat(
        fundCalculator.getDistributionsWithCalculatedPercents(fundsType1, Fund.Investment.SAFE),
        is(expectedDistributions));
  }

  @Test
  public void calculateNonIntegerPercentsTest() {
    List<FundDistribution> expectedDistributions = new ArrayList<>();
    expectedDistributions.add(new FundDistribution(fundsType2.get(0), null, 6.68));
    expectedDistributions.add(new FundDistribution(fundsType2.get(1), null, 6.66));
    expectedDistributions.add(new FundDistribution(fundsType2.get(2), null, 6.66));
    expectedDistributions.add(new FundDistribution(fundsType2.get(3), null, 37.5));
    expectedDistributions.add(new FundDistribution(fundsType2.get(4), null, 37.5));
    expectedDistributions.add(new FundDistribution(fundsType2.get(5), null, 5.0));

    assertThat(
        fundCalculator.getDistributionsWithCalculatedPercents(fundsType2, Fund.Investment.SAFE),
        is(expectedDistributions));
  }

  @Test
  public void fillFundsPercentagesIfIncompleteTest() {
    List<FundDistribution> inputDistributions = new ArrayList<>();
    inputDistributions.add(new FundDistribution(fundsType2.get(0), null, 6.66));
    inputDistributions.add(new FundDistribution(fundsType2.get(1), null, 6.66));
    inputDistributions.add(new FundDistribution(fundsType2.get(2), null, 6.66));
    inputDistributions.add(new FundDistribution(fundsType2.get(3), null, 37.5));
    inputDistributions.add(new FundDistribution(fundsType2.get(4), null, 37.5));
    inputDistributions.add(new FundDistribution(fundsType2.get(5), null, 5.0));

    List<FundDistribution> expectedDistributions = new ArrayList<>(inputDistributions);
    expectedDistributions.get(0).setPercent(6.68);

    assertThat(
        fundCalculator.fillFundsPercentagesIfIncomplete(
            inputDistributions, fundTypesPartsForInvestment),
        is(expectedDistributions));
  }

  @Test
  public void fillFundsPercentagesWhenAllIncompleteTest() {
    List<FundDistribution> inputDistributions = new ArrayList<>();
    inputDistributions.add(new FundDistribution(fundsType2.get(0), null, 6.0));
    inputDistributions.add(new FundDistribution(fundsType2.get(1), null, 6.0));
    inputDistributions.add(new FundDistribution(fundsType2.get(2), null, 6.0));
    inputDistributions.add(new FundDistribution(fundsType2.get(3), null, 37.0));
    inputDistributions.add(new FundDistribution(fundsType2.get(4), null, 37.0));
    inputDistributions.add(new FundDistribution(fundsType2.get(5), null, 3.0));

    List<FundDistribution> expectedDistributions = new ArrayList<>(inputDistributions);
    expectedDistributions.get(0).setPercent(8.0);
    expectedDistributions.get(3).setPercent(38.0);
    expectedDistributions.get(5).setPercent(5.0);

    assertThat(
        fundCalculator.fillFundsPercentagesIfIncomplete(
            inputDistributions, fundTypesPartsForInvestment),
        is(expectedDistributions));
  }
}
