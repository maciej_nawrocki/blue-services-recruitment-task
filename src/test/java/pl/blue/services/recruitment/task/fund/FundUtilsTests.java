package pl.blue.services.recruitment.task.fund;

import org.junit.Test;
import pl.blue.services.recruitment.task.model.Fund;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class FundUtilsTests {

  private static final double DELTA = 0.001;

  @Test
  public void countFundTypesTest() {
    List<Fund> inputFunds = new ArrayList<>();
    inputFunds.add(new Fund(1L, Fund.Type.POLISH, "name1"));
    inputFunds.add(new Fund(2L, Fund.Type.POLISH, "name2"));
    inputFunds.add(new Fund(3L, Fund.Type.FOREIGN, "name3"));
    inputFunds.add(new Fund(4L, Fund.Type.CASH, "name4"));

    Map<Fund.Type, Integer> expectedMap = new EnumMap<>(Fund.Type.class);
    expectedMap.put(Fund.Type.POLISH, 2);
    expectedMap.put(Fund.Type.FOREIGN, 1);
    expectedMap.put(Fund.Type.CASH, 1);

    assertThat(FundUtils.countFundTypes(inputFunds), is(expectedMap));
  }

  @Test
  public void roundValueFloorTest() {
    double expectedValue = 6.66;
    assertEquals(expectedValue, FundUtils.roundValueFloor(20.0 / 3), DELTA);
  }

  @Test
  public void roundValueFloorTest2() {
    double expectedValue = 10.0;
    assertEquals(expectedValue, FundUtils.roundValueFloor(10.0099), DELTA);
  }
}
