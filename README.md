#Blue services recruitment task

Aplikacja Java, implementująca zadanie opisane w pliku [ZADANIE JAVA_13032018.pdf](ZADANIE JAVA_13032018.pdf)

##Uruchomienie

Aplikacja uruchamiana jedynie z poziomu testów automatycznych poleceniem:
```
mvn test
```

##Wykorzystane narzędzia i technologie

- Java 8
- Spring Boot Framework 2.1.3.RELEASE
- Apache Maven 3.6.0
- Intellij IDEA